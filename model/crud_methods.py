# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.osv import orm
from openerp.tools.translate import _
import logging

logger = logging.getLogger(__name__)


SAFE_METHODS = [('create', 'Create'),
                ('read', 'Read (experimental!)'),
                ('write', 'Update (Write)'),
                ('unlink', 'Delete (Unlink)')]


def onchange_crud_type(self, cr, uid, ids, crud_type, model_id, context):
    domain = {'child_ids': []}
    to_ret = {'domain': domain, 'value': {'child_ids': []}}
    if not crud_type:
        return to_ret
    if crud_type in ['create', 'read']:
        domain['child_ids'] = ['&',
                               ('state', '!=', 'client_action'),
                               '|',
                               ('state', '!=', 'object_create'),
                               ('srcmodel_id', '!=', model_id)]

    return to_ret


def _initiate_method(self, cr, user, ids, method, context=None):
    context = context or {}
    server_pool = self.pool.get('ir.actions.server')
    action_ids = server_pool.search(
        cr, user, [
            ('model_id', '=', self._name),
            ('state', '=', 'crud'),
            ('crud_type', '=', method)],
        context=context
    )
    ctx = context.copy()
    ctx.update(active_model=self._name, active_id=ids[0], active_ids=ids)
    res = False
    for each in server_pool.browse(cr, user, action_ids, context=context):
        try:
            res = server_pool.run(cr, user, [x.id for x in each.child_ids],
                                  context=ctx)
        except (orm.except_orm, AssertionError) as e:
            logger.error(e)
            raise orm.except_orm(
                _("Error"), _("Error when initiating Server Action. Please "
                              "inspect server logs for more information!"))
    return res


def create(self, cr, user, vals, context=None):
    to_ret = super(orm.Model, self).create(cr, user, vals, context)
    _initiate_method(self, cr, user, [to_ret], 'create', context)
    return to_ret


def write(self, cr, user, ids, vals, context=None):
    to_ret = super(orm.Model, self).write(cr, user, ids, vals, context)
    _initiate_method(self, cr, user, ids, 'write', context)
    return to_ret


def read(self, cr, user, ids, fields=None, context=None, load='_classic_read'):
    to_ret = super(orm.Model, self).read(cr, user, ids, fields, context, load)
    _initiate_method(self, cr, user, ids, 'read', context)
    return to_ret


def unlink(self, cr, uid, ids, context=None):
    _initiate_method(self, cr, uid, ids, 'unlink', context)
    to_ret = super(orm.Model, self).unlink(cr, uid, ids, context)
    return to_ret
