# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.osv import orm, fields
from openerp import pooler
import crud_methods
from openerp import SUPERUSER_ID


class IrActionsServer(orm.Model):
    _inherit = 'ir.actions.server'

    _columns = {
        'state': fields.selection([
            ('client_action','Client Action'),
            ('dummy','Dummy'),
            ('loop','Iteration'),
            ('code','Python Code'),
            ('trigger','Trigger'),
            ('email','Email'),
            ('sms','SMS'),
            ('object_create','Create Object'),
            ('object_copy','Copy Object'),
            ('object_write','Write Object'),
            ('other','Multi Actions'),
            ('crud', 'CRUD action')
        ], 'Action Type', required=True, size=32, help="Type of the Action that is to be executed"),
        'crud_type': fields.selection(crud_methods.SAFE_METHODS, string="Method")
    }

    def _get_model_name_by_id(self, cr, model_id):
        sql = """
        SELECT model
        FROM ir_model
        WHERE id=%s
        """
        args = (model_id, )
        cr.execute(sql, args)
        model_name = cr.fetchone()
        return model_name and str(model_name[0].strip()) or None

    def override_method(self, cr, model_id, method):
        name = self._get_model_name_by_id(cr, model_id)
        if not name or (not method or method == 'False'):
            return None
        name_parts = name.split('.')
        class_name = ''.join([x.capitalize() for x in name_parts])
        class_name += '%s%s' % ('CRUD', method)
        # e.g. for res.partner name becomes ResPartnerCRUDcreate

        #TODO: make this inheritance friendly
        method_def = getattr(crud_methods, method)

        #TODO: ensure there's no memory-leak here
        obj = type(class_name, (orm.Model, ),
                   {'_inherit': name, method: method_def})
        obj.create_instance(self.pool, cr)

    def create(self, cr, user, vals, context=None):
        to_ret = super(IrActionsServer, self).create(cr, user, vals, context)
        if vals.get('state') == 'crud':
            self.override_method(
                cr, vals.get('model_id'), vals.get('crud_type'))
        return to_ret

    def unlink(self, cr, uid, ids, context=None):
        crud_actions = self.search(
            cr, uid, [('id', 'in', ids), ('state', '=', 'crud')],
            context=context
        )
        to_ret = super(IrActionsServer, self).unlink(cr, uid, ids, context)
        if crud_actions:
            pooler.restart_pool(cr.dbname)

        return to_ret

    def write(self, cr, user, ids, vals, context=None):
        to_ret = super(IrActionsServer, self).write(
            cr, user, ids, vals, context)
        if vals.get('state') == 'crud':
            pooler.restart_pool(cr.dbname)

        if 'crud_type' in vals:
            model_id = self.browse(cr, user, ids[0], context).model_id.id
            self.override_method(
                cr, vals.get('model_id', model_id), vals.get('crud_type'))
            pooler.restart_pool(cr.dbname)

        return to_ret

    def onchange_crud_type(self, cr, uid, ids, crud_type, model_id, context=None):
        return crud_methods.onchange_crud_type(
            self, cr, uid, ids, crud_type, model_id, context)

    def _register_hook(self, cr):
        crud_actions = self.search(cr, SUPERUSER_ID, [('state', '=', 'crud')])
        for each in self.read(cr, SUPERUSER_ID, crud_actions, ['model_id', 'crud_type']):
            self.override_method(cr, each['model_id'][0], str(each['crud_type']))