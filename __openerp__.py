# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

{
    'name': 'Base Server Action CRUD',
    'version': '0.1',
    'author': 'Versada UAB',
    'category': 'Other',
    'website': 'http://www.versada.lt',
    'description': """
Server action CRUD
======================

(C)reate, (R)ead, (U)pdate, (D)elete - CRUD - is new server action type which
let's you define which server actions to launch when one of CRUD action happens.

Example use cases:
    * Send an email/sms when new record (e.g. Task/Partner) is created;
    * Compute field value when one (or several) fields are changed (e.g. compute difference between two dates);
    * Duplicate new records;
    * etc.

Note: *Read* is experimental at this point. Also be careful with very advanced
setups because this module is in alpha state.
    """,
    'depends': [
        'base',
    ],
    'data': [
    ],
    'update_xml': [
        'view/ir.xml',
    ],
    'installable': True,
    'application': False,
}